console.log("Hello World");

// Asynchronous Statement
// The Fetch API allows to asynchronously request from a resource(data)
console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

fetch("https://jsonplaceholder.typicode.com/posts").then(response => console.log(response.status));

console.log("Goodbye");

// Retreive contents/data from the "Response" object
fetch("https://jsonplaceholder.typicode.com/posts").then((response) => response.json()).then((json) => console.log(json));

/*"async" and "await"*/
// GET Method
async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");

	// Result returned by fetch is a returned promise
	console.log(result);

	// The returned "Response" is an object
	console.log(typeof result);

	// To access the content of the "Response", body property should be directly accessed
	console.log(result.body);

	let json = await result.json();

	console.log(json);
};

fetchData();

// Getting a specific post
fetch("https://jsonplaceholder.typicode.com/posts/1").then((response => response.json())).then((json => console.log(json)));

/*Creating a post (POST)
	
	Syntax:
	fetch("URL", options).then((response) =>{}).then((response) => {});
*/
fetch("https://jsonplaceholder.typicode.com/posts",{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

/*Update a post (PUT)
	Syntax:
	fetch("URL", options).then((response) =>{}).then((response) => {});
*/
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
			id: 1,
			title: "Updated Post",
			body: "Hello Again!",
			userId: 1
		})
}).then((response) => response.json()).then((json) => console.log(json));

// Update a post using PATCH Method
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
			title: "Corrected Post",
		})
}).then((response) => response.json()).then((json) => console.log(json));

/*Deleting a post*/
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "DELETE"
});

























































































































































































